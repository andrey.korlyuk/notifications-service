import sbt._
import sbt.Keys._

object Commons {

  val settings: Seq[Def.Setting[_]] = Seq(
    scalaVersion := "2.13.3",
    scalacOptions := Seq(
      "-encoding",
      "UTF-8", // yes, this is 2 args
      "-deprecation",
      "-feature",
      "-unchecked",
      "-language:implicitConversions"
    ),
    resolvers in ThisBuild ++= Seq(
      "Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/",
      "Radicalbit Releases" at "https://tools.radicalbit.io/artifactory/libs-release-local/",
      "Radicalbit Snapshots" at "https://tools.radicalbit.io/artifactory/libs-snapshot-local/",
      Resolver.bintrayRepo("cakesolutions", "maven"),
      "confluent" at "https://packages.confluent.io/maven/",
      Resolver.mavenLocal
    ),
    credentials ++= Seq(Credentials(Path.userHome / ".artifactory" / ".credentials")),
    // some options to facilitate tests
    fork in Test := true,
    parallelExecution in Test := false,
    testForkedParallel in Test := false,
    javaOptions in Test += "-Xms2048m",
    javaOptions in Test += "-Xmx2048m",
    javaOptions in Test += "-XX:ReservedCodeCacheSize=256m",
    javaOptions in Test += "-XX:MaxMetaspaceSize=512m"
  )

}
