import sbt._

object Dependencies {

  private object mockito {
    lazy val version   = "3.3.3"
    lazy val namespace = "org.mockito"
    lazy val core      = namespace % "mockito-core" % version
  }

  private object scalatestplus {
    lazy val version   = "5.0.0"
    lazy val namespace = "org.scalatestplus.play"
    lazy val core      = namespace %% "scalatestplus-play" % version
  }

  private object logback {
    lazy val version   = "1.2.3"
    lazy val namespace = "ch.qos.logback"
    lazy val classic   = namespace % "logback-classic" % version
  }

  private object radicalbit {
    private val namespace                = "io.radicalbit"
    private val kafkaAppenderVersion     = "1.0.0"
    private val securityEventsLibVersion = "1.3.1-SNAPSHOT"
    private val securityLibVersion       = "1.0.4-SNAPSHOT"
    private val protocolsLibVersion      = "1.0.1-SNAPSHOT"
    lazy val lsProtocolsLibVersion    = "1.0.1-SNAPSHOT"
    lazy val lsProtocolslib           = namespace %% "ls-protocols-lib" % lsProtocolsLibVersion
    lazy val kafkaAppender               = namespace  % "logback-kafka-appender"    % kafkaAppenderVersion
    lazy val playSecurityLib             = namespace %% "rtsae-security-lib-play"   % securityLibVersion
    lazy val protocolslib                = namespace %% "rtsae-protocols-lib"       % protocolsLibVersion
    lazy val securityEventsLib           = namespace %% "rtsae-security-events-lib" % securityEventsLibVersion
    lazy val exceptionsLib               = namespace %% "rna-exceptions-lib"        % "2.0.15"
  }

  private object kafka {
    lazy val version   = "2.5.1"
    lazy val namespace = "org.apache.kafka"
    lazy val core      = namespace %% "kafka" % version
  }

  private object embeddedKafka {
    lazy val version            = "5.5.1"
    lazy val namespace          = "io.github.embeddedkafka"
    lazy val withSchemaRegistry = namespace %% "embedded-kafka-schema-registry" % version
  }

  private object alpakka {
    private val version   = "2.0.5"
    private val namespace = "com.typesafe.akka"
    lazy val kafka        = namespace %% "akka-stream-kafka" % version
  }

  private object akka {
    private val version         = "2.6.5"
    private val namespace       = "com.typesafe.akka"
    lazy val akkaStreamsTestkit = namespace %% "akka-stream-testkit" % version
    lazy val akkaTestkit        = namespace %% "akka-testkit"        % version
  }

  private object awaitility {
    private val version        = "4.0.2"
    private lazy val namespace = "org.awaitility"
    lazy val core              = namespace % "awaitility-scala" % version
  }

  private object kamon {
    lazy val namespace  = "io.kamon"
    lazy val bundle     = namespace %% "kamon-bundle" % "2.1.9"
    lazy val prometheus = namespace %% "kamon-prometheus" % "2.1.9"
  }

  lazy val libraries = Seq(
    logback.classic,
    radicalbit.kafkaAppender exclude ("org.apache.kafka", "kafka-clients"),
    radicalbit.playSecurityLib,
    radicalbit.protocolslib,
    radicalbit.securityEventsLib,
    radicalbit.exceptionsLib,
    kafka.core,
    alpakka.kafka,
    kamon.bundle,
    kamon.prometheus,
    radicalbit.lsProtocolslib,
    // Test libs
    mockito.core                     % "it,test",
    scalatestplus.core               % "it,test",
    embeddedKafka.withSchemaRegistry % "it,test",
    awaitility.core                  % "it,test",
    akka.akkaTestkit                 % Test,
    akka.akkaStreamsTestkit          % Test
  )

}
