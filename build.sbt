import sbt._
import com.typesafe.sbt.packager.docker.{Cmd, ExecCmd}

resolvers += "jitpack" at "https://jitpack.io"

addCommandAlias("verify", ";clean;coverage;test")

lazy val `rtsae-notifications-service` = (project in file("."))
  .settings(Commons.settings: _*)
  .enablePlugins(PlayScala, JavaAppPackaging, BuildInfoPlugin, DockerPlugin, DockerComposePlugin)
  .disablePlugins(PlayLayoutPlugin)
  .configs(IntegrationTest)
  .settings(
    // project info
    organization := "io.radicalbit",
    name := "rtsae-notifications-service",
    // dependencies
    libraryDependencies ++= Dependencies.libraries ++ Seq(guice, ws, ehcache),
    // play settings
    PlayKeys.playMonitoredFiles ++= (sourceDirectories in (Compile, TwirlKeys.compileTemplates)).value,
    // format code
    scalafmtOnCompile := true,
    // integration tests settings
    Defaults.itSettings,
    inConfig(IntegrationTest.extend(Test))(org.scalafmt.sbt.ScalafmtPlugin.projectSettings)
  )
  .settings(
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "io.radicalbit.rna",
    buildInfoOptions += BuildInfoOption.ToJson
  )
  .settings(
    packageName in Docker := packageName.value,
    version in Docker := version.value,
    maintainer in Docker := organization.value,
    dockerRepository := Some("tools.radicalbit.io"),
    defaultLinuxInstallLocation in Docker := s"/opt/${packageName.value}",
    dockerCommands := Seq(
      Cmd("FROM", "tools.radicalbit.io/service-java-base:1.2"),
      Cmd("LABEL", s"""MAINTAINER="${organization.value}""""),
      Cmd("WORKDIR", s"/opt/${packageName.value}"),
      Cmd("ADD", "opt", "/opt"),
      Cmd("RUN", "addgroup", "-S", "rna", "&&", "adduser", "-S", "rna", "-G", "rna"),
      ExecCmd("RUN", "chown", "-R", "rna:rna", "."),
      Cmd("HEALTHCHECK", "--timeout=3s", "CMD", "curl", "-f", "http://localhost:9000/healthcheck || exit 1"),
      Cmd("CMD", s"bin/${packageName.value}")
    ),
    dockerImageCreationTask := (publishLocal in Docker).value,
    variablesForSubstitution := Map(
      "image_name" -> s"${dockerRepository.value.get}/${packageName.value}:${version.value}"
    )
  )

// Otherwise it runs kafka for each suite and throws 'address already in use' error
parallelExecution in IntegrationTest := false

// To avoid "This application is already running (Or delete /opt/docker/RUNNING_PID file)."
javaOptions in Universal ++= Seq(
  "-Dpidfile.path=/dev/null"
)

// avoid saving temporary files to /var/folders on macOS
javaOptions ++= Seq(
  "-Djava.io.tmpdir=/tmp"
)
