# RTSAE notifications service #

### How to use swagger ###
* This project's APIs make use of swagger specification, a set of rules used to describe and 
  document RESTful APIs
* To access the swagger-ui, visit `/swagger`
* To read the yaml and json files, visit `/api-docs/swagger.yaml` and `/api-docs/swagger.json`
* Fore more info, visit http://swagger.io/specification

### How do I build the docker image? ###
* Launch sbt
* Use `docker:publishLocal` to build the image using the local Docker server
* Use `docker:publish` do build the image using the local Docker server and then push it to the remote
  configured repository (this procedure should be done by Jenkins and not locally except for specific requirements).
* To remove the image from the local Docker server, run `docker:clean`

### How to test the service during development
There are two ways:
* run `dockerComposeUp` to start the services in the `docker-compose.yml` file, and `dockerComposeStop` to stop the services
* run `docker-compose -f docker-compose-external.yml up -d` to start all the containers required by the service and then run the service
  with sbt. To stop the required containers run `docker-compose -f docker-compose-external.yml down`.

### Logging
Three different logback configurations are present into configurations directory:

* `logback.xml`: it uses only an async console appender, it is used as default for develop
* `logback-file.xml`: it uses async appenders for console and file. It could be used for standalone installations.
* `logback-kafka.xml`: it uses async appenders for console and kafka producers. It could be used for standalone (but no log files are produced) or docker installations.

#### Logging in our production environment
In production we use docker, so `logback-kafka.xml` configuration is the right choice, to write all logs to kafka and don't write files into running container.
Two env var must be used to configure the Kafka appender:

* `LOG_KAFKA_BROKERS`: the kafka bootstrap servers used by the producer (default `localhost:9092`)
* `LOG_KAFKA_TOPIC`: the topic where the logs must be written (default `logs`)

It is also possible to override `LOG_LEVEL` property (default `INFO`) the define the log level.

Also the application should be executed with the right logback configuration using `-Dlogger.file=/path/to/conf/logback-kafka.xml`

For example a docker configuration could be:

```yaml
  my-service:
    image: tools.radicalbit.io/my-service
    environment:
      ...
      LOG_KAFKA_BROKERS: borker1:9092,broker2:9092,broker3:9092
      LOG_KAFKA_TOPIC: logs
      LOG_LEVEL: DEBUG
    command: application-start-command -Dlogger.file=/path/to/conf/logback-kafka.xml
```