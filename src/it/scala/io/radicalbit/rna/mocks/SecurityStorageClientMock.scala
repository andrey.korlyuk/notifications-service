package io.radicalbit.rna.mocks

import java.util.UUID

import io.radicalbit.rna.client.SecurityStorageClient
import io.radicalbit.rna.models.SubtenantSecurity
import javax.inject.Singleton

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SecurityStorageClientMock extends SecurityStorageClient {

  override implicit val ec: ExecutionContext = ExecutionContext.global

  override def getAdminSubtenants: Future[Seq[SubtenantSecurity]] = Future(SecurityStorageClientMock.adminSubtenants)
}

object SecurityStorageClientMock {

  val subTenant1UUID = "20d9d911-1c72-442b-bc9e-00786ca31ebb"
  val subTenant2UUID = "f24711d9-4807-4238-afd9-3720bc0cc8a5"

  lazy val adminSubtenants: Seq[SubtenantSecurity] =
    Seq(
      SubtenantSecurity(UUID.fromString(subTenant1UUID), "adminSubtenant1", "accessLevel", Seq.empty),
      SubtenantSecurity(UUID.fromString(subTenant2UUID), "adminSubtenant2", "accessLevel", Seq.empty)
    )

}

