package io.radicalbit.rna.mocks

import io.radicalbit.rtsae.security.clients.KeycloakClient
import io.radicalbit.rtsae.security.models.{KeyData, Keys}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class KeycloakClientMock extends KeycloakClient {

  override def publicKeys(url: String): Future[Keys] =
    Future(Keys(keys = Seq(KeyData(kid = "3RiKYSHQgtYrzJAM16aTU8meqgMEg2wwwD5zga9CK2o", n = "n", e = "e"))))
}
