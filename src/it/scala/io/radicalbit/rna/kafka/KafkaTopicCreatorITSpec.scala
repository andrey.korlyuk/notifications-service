package io.radicalbit.rna.kafka

import java.util.Properties

import com.typesafe.config.{Config, ConfigFactory}
import io.radicalbit.rtsae.security.clients.KeycloakClient
import net.manub.embeddedkafka.schemaregistry.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.apache.kafka.clients.admin.{AdminClient, AdminClientConfig}
import org.scalatest.{BeforeAndAfterAll, MustMatchers, WordSpecLike}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.{Application, Mode}

import scala.jdk.CollectionConverters._
import scala.reflect.ClassTag

class KafkaTopicCreatorITSpec extends WordSpecLike with MustMatchers with GuiceOneAppPerSuite
  with EmbeddedKafka
  with BeforeAndAfterAll with MockitoSugar {

  protected val keycloakClient                 =  mock[KeycloakClient]

  val configuration: Config = ConfigFactory.load(classOf[KafkaTopicCreatorITSpec].getClassLoader)

  val dummyTopics = List("topic1", "topic2")

  override def fakeApplication(): Application = {

    val fakeApplication = new GuiceApplicationBuilder()
      .in(Mode.Test)
      .configure(
        "kafka.bootstrap.servers" -> s"localhost:${implicitly[EmbeddedKafkaConfig].kafkaPort}",
        "kafka.replicas" -> 1,
        "kafka.topic.names" -> dummyTopics.mkString(",")
      )
      .overrides(bind[KeycloakClient].to(keycloakClient))
      .build

    fakeApplication
  }
  protected def getComponent[A: ClassTag]: A = fakeApplication.injector.instanceOf[A]

  if(!EmbeddedKafka.isRunning) EmbeddedKafka.start()

  override def afterAll(): Unit = {
    super.afterAll()
    EmbeddedKafka.stop()
  }

  "NotificationListener" should {
    "create topics" in {
      val configs = implicitly[EmbeddedKafkaConfig]

      val props = new Properties()
      props.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, s"localhost:${configs.kafkaPort}")
      val adminClient = AdminClient.create(props)
      adminClient
        .listTopics()
        .names()
        .get()
        .containsAll(dummyTopics.asJava) mustBe true
      adminClient.close()
    }
  }
}
