package io.radicalbit.rna.controllers

import java.time.Instant
import java.util.UUID
import java.util.function.Consumer

import io.radicalbit.rna.specs.BaseITSpec
import io.radicalbit.rtsae.kafka.models.{Envelope, PayloadSample}
import io.radicalbit.rtsae.kafka.serde.RnaSerde.{AvroValueDeserialiser, AvroValueSerialiser}
import io.radicalbit.rtsae.security.models.{All, ManageWidgets}
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.common.serialization.{Deserializer, Serializer}
import org.awaitility.Awaitility._
import org.awaitility.core.ConditionTimeoutException
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.PatienceConfiguration.Timeout
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import play.api.libs.ws.WSClient
import play.api.test.{TestServer, WsTestClient}
import play.shaded.ahc.org.asynchttpclient.AsyncHttpClient

import scala.collection.mutable
import scala.compat.java8.FutureConverters
import scala.concurrent.duration.DurationInt

class NotificationControllerSpec extends PlaySpec with BaseITSpec with ScalaFutures with BeforeAndAfterAll {

  private val srURL = schemaRegistryConf.url

  implicit val EnvelopeDeserialiser: Deserializer[Envelope] =
    AvroValueDeserialiser(srURL, useSpecificReader = false).getUnderlying[Envelope]

  implicit val PayloadSerialiser: Serializer[GenericRecord] =
    AvroValueSerialiser(srURL).getUnderlying[GenericRecord]

  implicit val EnvelopeSerialiser: Serializer[Envelope] = AvroValueSerialiser(srURL).getUnderlying[Envelope]

  private val server = TestServer(port, fakeApplication)

  private val wsPublicEndpoint = s"ws://localhost:$port/topics/public/TEST"

  private val wsRestrictedEndpoint = s"ws://localhost:$port/topics/restricted/TEST"


  override def beforeAll(): Unit = {
    super.beforeAll()
    server.start()
  }

  override def afterAll(): Unit = {
    super.afterAll()
    server.stop()
  }

  def createMessage(key: String, isPublic: Boolean, permits: Seq[String] = Seq.empty, uuid: UUID = UUID.randomUUID(), subtenantUUID: UUID = TenantUuid) = {
    val msg = PayloadSample(uuid, "pippo_franco")
    val serMsg = AvroValueSerialiser(srURL).serialize(kafkaConf.topics.head, msg)
    Envelope(None, subtenantUUID, key, Instant.now(), isPublic, permits, serMsg)
  }

  "NotificationController" when {
    "Serving a public topic" should {
      "accept public websocket flow" in WsTestClient.withClient { client =>

        val uuid = UUID.randomUUID()
        val envelope = createMessage("TEST", true, uuid = uuid)
        testWebSoket(client, wsPublicEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelope)
          await().until(() => queue.nonEmpty)
          val input: String = queue.dequeue()
          input mustBe s"""{"id":"$uuid","fullName":"pippo_franco"}"""
        }
      }
      "broadcast only public messages" in WsTestClient.withClient { client =>
        val uuid1 = UUID.randomUUID()
        val uuid2 = UUID.randomUUID()
        val envelopePublic = createMessage("TEST", true, uuid = uuid1)
        val envelopePrivate = createMessage("TEST", false, uuid = uuid2)

        testWebSoket(client, wsPublicEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelopePublic)
          publishToKafka(kafkaConf.topics.head, envelopePrivate)
          await().until(() => queue.nonEmpty)
          queue must have size 1
          val input: String = queue.dequeue()
          input mustBe s"""{"id":"$uuid1","fullName":"pippo_franco"}"""
        }
      }

      "broadcast only messages with correct key" in WsTestClient.withClient { client =>
        val uuid1 = UUID.randomUUID()
        val uuid2 = UUID.randomUUID()
        val envelope = createMessage("TEST", true, uuid = uuid1)
        val envelopePrivateIgnore = createMessage("TEST-IGNORE", false, uuid = uuid2)

        testWebSoket(client, wsPublicEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelope)
          publishToKafka(kafkaConf.topics.head, envelopePrivateIgnore)
          await().until(() => queue.nonEmpty)
          queue must have size 1
          val input: String = queue.dequeue()
          input mustBe s"""{"id":"$uuid1","fullName":"pippo_franco"}"""
        }
      }

      "listen to multiple topics" in WsTestClient.withClient { client =>
        val uuid1 = UUID.randomUUID()
        val uuid2 = UUID.randomUUID()

        val envelopeTopic1 = createMessage("TEST", true, uuid = uuid1)
        val envelopeTopic2 = createMessage("TEST", true, uuid = uuid2)

        testWebSoket(client, wsPublicEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelopeTopic1)
          publishToKafka(kafkaConf.topics.last, envelopeTopic2)
          await().until(() => queue.nonEmpty)
          queue must have size 2
          queue.dequeue() mustBe s"""{"id":"$uuid1","fullName":"pippo_franco"}"""
          queue.dequeue() mustBe s"""{"id":"$uuid2","fullName":"pippo_franco"}"""
        }
      }
    }
    "serving restricted topic" should {

      "reject restricted topic channel if token is absent" in WsTestClient.withClient { client =>
        val asyncHttpClient: AsyncHttpClient = client.underlying[AsyncHttpClient]
        val webSocketClient = new WebSocketClient(asyncHttpClient)
        val queue = new mutable.Queue[String](10)

        val consumer: Consumer[String] = (message: String) => queue.enqueue(message)
        val listener = new WebSocketClient.LoggingListener(consumer)

        val completionStage = webSocketClient.call(wsRestrictedEndpoint, wsRestrictedEndpoint, listener, "")
        val f = FutureConverters.toScala(completionStage)
        whenReady(f, timeout = Timeout(5.second)) { webSocket =>
          val maybeError = listener.getThrowable
          maybeError.getMessage mustBe "Invalid Status code=400 text=Bad Request"
        }
      }
      "open websocket for consuming messages if token is valid" in WsTestClient.withClient { client =>
        val uuid = UUID.randomUUID()
        val envelope = createMessage("TEST", false, permits = Seq(All.code), uuid = uuid)

        testWebSoket(client, wsRestrictedEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelope)

          await().until(() => queue.nonEmpty)
          queue must have size 1
          val input: String = queue.dequeue()
          input mustBe s"""{"id":"$uuid","fullName":"pippo_franco"}"""
        }
      }

      "discard messages if subtenant doesn't match" in WsTestClient.withClient { client =>
        val uuid = UUID.randomUUID()
        val envelope = createMessage("TEST", false, permits = Seq(All.code), uuid = uuid, subtenantUUID = UUID.randomUUID())

        testWebSoket(client, wsRestrictedEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelope)

          assertThrows[ConditionTimeoutException] {
            await().until(() => queue.nonEmpty)
          }
        }
      }
      "accept messages if subtenant doesn't match, but the client is Admin" in WsTestClient.withClient { client =>
        val uuid = UUID.randomUUID()
        val envelope = createMessage("TEST", false, permits = Seq(All.code), uuid = uuid, subtenantUUID = UUID.randomUUID())

        testWebSoket(client, wsRestrictedEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelope)

          assertThrows[ConditionTimeoutException] {
            await().until(() => queue.nonEmpty)
          }
        }
      }
      "discard messages if required permit is absent" in WsTestClient.withClient { client =>

        val uuid = UUID.randomUUID()
        val envelope = createMessage("TEST", false, permits = Seq(ManageWidgets.code), uuid = uuid)

        testWebSoket(client, wsRestrictedEndpoint) { queue =>
          publishToKafka(kafkaConf.topics.head, envelope)
          assertThrows[ConditionTimeoutException] {
            await().until(() => queue.nonEmpty)
          }
        }
      }
    }
    "filtering of messages by query string" should {

      "return messages which satisfy condition" in WsTestClient.withClient { client =>
          val expectedUuid = UUID.randomUUID()
          val envelope1 = createMessage("TEST", true, permits = Seq(All.code), uuid = expectedUuid, subtenantUUID = UUID.randomUUID())
          val envelope2 = createMessage("TEST", true, permits = Seq(All.code), uuid = UUID.randomUUID(), subtenantUUID = UUID.randomUUID())
        testWebSoket(client, s"$wsRestrictedEndpoint?id=$expectedUuid") { queue =>
          publishToKafka(kafkaConf.topics.head, envelope1)
          publishToKafka(kafkaConf.topics.head, envelope2)

          await().until(() => queue.nonEmpty)
          queue must have size 1
          val input: String = queue.dequeue()
          input mustBe s"""{"id":"$expectedUuid","fullName":"pippo_franco"}"""
        }
    }
      "skip message if filtered field is not present in the message" in WsTestClient.withClient { client =>
        val envelope1 = createMessage("TEST", true, permits = Seq(All.code), uuid = UUID.randomUUID(), subtenantUUID = UUID.randomUUID())
        val envelope2 = createMessage("TEST", true, permits = Seq(All.code), uuid = UUID.randomUUID(), subtenantUUID = UUID.randomUUID())
        testWebSoket(client, s"$wsRestrictedEndpoint?test=test1") { queue =>
          publishToKafka(kafkaConf.topics.head, envelope1)
          publishToKafka(kafkaConf.topics.head, envelope2)

          await().until(() => queue.nonEmpty)
          assertThrows[ConditionTimeoutException] {
            await().until(() => queue.nonEmpty)
          }
        }
      }
      "apply logical conjunction to filter" in WsTestClient.withClient { client =>
        val expectedUuid = UUID.randomUUID()
        val envelope1 = createMessage("TEST", true, permits = Seq(All.code), uuid = expectedUuid, subtenantUUID = UUID.randomUUID())
        val envelope2 = createMessage("TEST", true, permits = Seq(All.code), uuid = UUID.randomUUID(), subtenantUUID = UUID.randomUUID())
        testWebSoket(client, s"$wsRestrictedEndpoint?id=$expectedUuid&fullName=pippo_franco") { queue =>
          publishToKafka(kafkaConf.topics.head, envelope1)
          publishToKafka(kafkaConf.topics.head, envelope2)

          await().until(() => queue.nonEmpty)
          queue must have size 1
          val input: String = queue.dequeue()
          input mustBe s"""{"id":"$expectedUuid","fullName":"pippo_franco"}"""
        }
      }
    }
  }

  private def testWebSoket[T](client: WSClient, endpoint: String)(test: mutable.Queue[String] => T) = {
    val asyncHttpClient: AsyncHttpClient = client.underlying[AsyncHttpClient]
    val webSocketClient = new WebSocketClient(asyncHttpClient)
    val queue = new mutable.Queue[String](10)

    val consumer: Consumer[String] = (message: String) => queue.enqueue(message)
    val listener = new WebSocketClient.LoggingListener(consumer)

    val completionStage = webSocketClient.call(endpoint, endpoint, listener, token().tokenize)
    val f = FutureConverters.toScala(completionStage)
    whenReady(f, timeout = Timeout(5.second)) { webSocket =>
      val maybeError = Option(listener.getThrowable)
      maybeError mustBe None

      await().until(() => webSocket.isOpen)
      Thread.sleep(7000) // ugly, but client needs time before it can consume message

      test
    }

  }
}
