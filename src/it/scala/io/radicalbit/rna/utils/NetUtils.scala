package io.radicalbit.rna.utils

import java.io.IOException
import java.net.ServerSocket

object NetUtils {

  def getAvailablePort: Int = {
    var i = 0
    while ({ i < 50 }) {
      try {
        val serverSocket = new ServerSocket(0)
        try {
          val port = serverSocket.getLocalPort
          if (port != 0) return port
        } catch {
          case _: IOException =>
        } finally {
          if (serverSocket != null) serverSocket.close()
        }
      }

      { i += 1; i - 1 }
    }
    throw new RuntimeException("Could not find a free permitted port on the machine.")
  }
}
