package io.radicalbit.rna.specs

import java.util.UUID

import com.typesafe.config.Config
import io.radicalbit.rna.client.SecurityStorageClient
import io.radicalbit.rna.conf.ApplicationConfig.{KafkaConfigFactory, SchemaRegistryConfigFactory}
import io.radicalbit.rna.mocks.{KeycloakClientMock, SecurityStorageClientMock}
import io.radicalbit.rna.utils.NetUtils
import io.radicalbit.rtsae.security.clients.KeycloakClient
import io.radicalbit.rtsae.security.models.{All, KeyData, Keys, RnaSecurityFields, RnaToken, TokenHeader, TokenPayload}
import net.manub.embeddedkafka.schemaregistry.{EmbeddedKafka, EmbeddedKafkaConfig}
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.when
import org.scalatest._
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.{Application, Configuration, Mode}


trait BaseITSpec extends WordSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with MockitoSugar with EmbeddedKafka with GuiceOneAppPerSuite { this: Suite =>

  protected val keycloakClient                 =  new KeycloakClientMock()
  protected lazy val securityStorageClientMock                = new SecurityStorageClientMock()

  val TenantUuidString      = "00000000-0000-0000-0000-000000000002"
  val AdminTenantUUID       = UUID.fromString(SecurityStorageClientMock.subTenant1UUID)
  val TenantUuid: UUID      = UUID.fromString(TenantUuidString)
  val TenantGroupUuid: UUID = UUID.fromString("00000000-0000-0000-0000-000000000098")

  def token(subtenant: UUID = TenantUuid): RnaToken = RnaToken(
    header = TokenHeader(
      alg = "RSA256",
      typ = "JWT",
      kid = "3RiKYSHQgtYrzJAM16aTU8meqgMEg2wwwD5zga9CK2o"
    ),
    payload = TokenPayload(
      exp = System.currentTimeMillis / 1000 + 1000,
      iat = System.currentTimeMillis / 1000 - 1000,
      nbf = 0L,
      rna = RnaSecurityFields(
        username = "username",
        subtenant = subtenant,
        subtenantName = "tenant",
        subtenantDisplayName = "tenant",
        groups = Seq(TenantGroupUuid),
        groupsName = Seq("groupOne"),
        permits = Seq(All.code)
      )
    )
  )

  if(!EmbeddedKafka.isRunning) EmbeddedKafka.start()

  val dummyTopics = List("topic1", "topic2")

  val port: Int = NetUtils.getAvailablePort

  override lazy val fakeApplication: Application = new GuiceApplicationBuilder()
    .in(Mode.Test)
    .configure(
      "kafka.bootstrap.servers" -> s"localhost:${implicitly[EmbeddedKafkaConfig].kafkaPort}",
      "kafka.replicas" -> 1,
      "kafka.partitions" -> 1,
      "kafka.topic.names" -> dummyTopics.mkString(","),
      "schema-registry.url" -> s"http://localhost:${implicitly[EmbeddedKafkaConfig].schemaRegistryPort}"
    )
    .overrides(bind[SecurityStorageClient].to(securityStorageClientMock))
    .overrides(bind[KeycloakClient].to(keycloakClient))
    .build()

  private def config(app: Application): Config = app.configuration.underlying

  val configuration: Configuration = Configuration(config(fakeApplication))

  val kafkaConf: KafkaConfigFactory.KafkaConfig = KafkaConfigFactory(configuration)

  val schemaRegistryConf: SchemaRegistryConfigFactory.SchemaRegistryConfig = SchemaRegistryConfigFactory(configuration)

  override def afterAll(): Unit = {
    super.afterAll()
    EmbeddedKafka.stop()
  }

}
