package io.radicalbit.rna.modules

import com.google.inject.AbstractModule
import io.radicalbit.rna.client.{SecurityStorageClient, SecurityStorageClientImpl}
import io.radicalbit.rna.kafka.KafkaTopicCreator
import io.radicalbit.rtsae.security.clients.KeycloakClient
import io.radicalbit.rtsae.security.clients.impl.KeycloakClientImpl
import play.api.{Configuration, Environment}

class BootstrapModule(environment: Environment, configuration: Configuration) extends AbstractModule {

  override def configure(): Unit = {
    bind(classOf[SecurityStorageClient]).to(classOf[SecurityStorageClientImpl])
    bind(classOf[KeycloakClient]).to(classOf[KeycloakClientImpl])
    bind(classOf[KafkaTopicCreator]).asEagerSingleton()
  }
}
