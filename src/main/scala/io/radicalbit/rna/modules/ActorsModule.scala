package io.radicalbit.rna.modules

import akka.actor.SupervisorStrategyConfigurator
import com.google.inject.{AbstractModule, Provides}
import io.radicalbit.rna.actors.{AugmentedDefaultSupervisionStrategy, WsMonitor}
import play.api.libs.concurrent.AkkaGuiceSupport

class ActorsModule extends AbstractModule with AkkaGuiceSupport {

  override def configure(): Unit = {
    bindActor[WsMonitor]("ws-monitor-actor")

    @Provides
    def supervisionStrategyConfiguratorProvider(): SupervisorStrategyConfigurator =
      new AugmentedDefaultSupervisionStrategy
  }

}
