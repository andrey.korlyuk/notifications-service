package io.radicalbit.rna.filters

import javax.inject.Inject
import akka.stream.Materializer
import org.slf4j
import play.api.Logger
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

class LoggingFilter @Inject() (implicit val mat: Materializer, ec: ExecutionContext) extends Filter {

  final lazy val logger: slf4j.Logger = Logger(this.getClass).logger

  def apply(next: RequestHeader => Future[Result])(request: RequestHeader): Future[Result] = {

    val requestTimestamp = System.currentTimeMillis()

    next(request).map { result =>
      val responseTimestamp = System.currentTimeMillis()
      logger.info(
        s"""REQUESTED:
           |method=${request.method}
           |uri=${request.uri}
           |remote-address=${request.remoteAddress}
           |headers=${request.headers}
           |query-string=${request.queryString.toString()}
           |response-status=${result.header.status}
           |response-time=${responseTimestamp - requestTimestamp}ms""".stripMargin
          .replaceAll(System.lineSeparator(), " - ")
      )
      result
    }
  }
}
