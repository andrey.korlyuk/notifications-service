package io.radicalbit.rna.kafka

import com.google.inject.{Inject, Singleton}
import io.radicalbit.rna.conf.ApplicationConfig.KafkaConfigFactory
import io.radicalbit.rna.service.BaseService
import io.radicalbit.rtsae.security.kafka.admin.{InternalTopicManager, TopicRetentionMs}
import play.api.Configuration

import scala.concurrent.ExecutionContext

@Singleton
class KafkaTopicCreator @Inject() (configuration: Configuration)(implicit
    ec: ExecutionContext
) extends BaseService {

  private val kafkaConf = KafkaConfigFactory(configuration)

  kafkaConf.topics.foreach { topic =>
    log.info(s"Creating topic $topic")
    log.info(s"Server: ${kafkaConf.bootstrapServers}")
    InternalTopicManager.createTopic(
      bootstrapServers = kafkaConf.bootstrapServers,
      topic = topic,
      partitions = kafkaConf.topicPartitions,
      replicationFactor = kafkaConf.topicReplicas,
      config = Map(TopicRetentionMs -> kafkaConf.topicRetentionMs.toString)
    )
  }
}
