package io.radicalbit.rna.models

import java.util.UUID

import play.api.libs.json.{Json, OFormat}

object GroupSecurity {
  implicit val format: OFormat[GroupSecurity] = Json.format[GroupSecurity]
}

case class GroupSecurity(id: UUID, name: String, accessLevel: String)

object SubtenantSecurity {
  implicit val format: OFormat[SubtenantSecurity] = Json.format[SubtenantSecurity]
}

case class SubtenantSecurity(id: UUID, name: String, accessLevel: String, groups: Seq[GroupSecurity])
