package io.radicalbit.rna.client

import java.util.UUID

import io.radicalbit.rna.models.SubtenantSecurity

import scala.concurrent.{ExecutionContext, Future}

trait SecurityStorageClient {

  implicit def ec: ExecutionContext

  def getAdminSubtenants: Future[Seq[SubtenantSecurity]]

  def isAdmin(subtenantId: UUID): Future[Boolean] = getAdminSubtenants.map(_.exists(_.id == subtenantId))
}
