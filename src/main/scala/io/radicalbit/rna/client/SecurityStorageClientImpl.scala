package io.radicalbit.rna.client

import io.radicalbit.rna.conf.ApplicationConfig.SecurityStorageService
import io.radicalbit.rna.models.SubtenantSecurity
import io.radicalbit.rtsae.security.models.{All, AllSubtenants, RnaSecurityFields}
import javax.inject.{Inject, Singleton}
import org.slf4j
import play.api.Logger
import play.api.libs.json.Json
import play.api.libs.ws.WSClient

import scala.concurrent.{ExecutionContext, Future}

object SecurityStorageClientImpl {
  class SecurityStorageException(msg: String) extends RuntimeException(msg)
}

@Singleton()
class SecurityStorageClientImpl @Inject() (ws: WSClient)(implicit val ec: ExecutionContext)
    extends SecurityStorageClient {

  private val logger: slf4j.Logger = Logger(classOf[SecurityStorageClientImpl]).logger

  def getAdminSubtenants: Future[Seq[SubtenantSecurity]] = {
    logger.info("Asking admin subtenants to storage service")
    ws.url(SecurityStorageService.activeAdminSubtenantsUrl)
      .get()
      .map { response =>
        Json.parse(response.body).as[Seq[SubtenantSecurity]]
      }
  }

  def isAdmin(rnaSecurityFields: RnaSecurityFields): Future[Boolean] =
    getAdminSubtenants.map(_.exists(_.id == rnaSecurityFields.subtenant)).map { isAdmin =>
      logger.debug(s"Current subtenant has one group with admin role: $isAdmin")
      val userHasPermits = rnaSecurityFields.permits.contains(All.code) || rnaSecurityFields.permits
        .contains(AllSubtenants.code)
      logger.debug(s"User has admin permits: $userHasPermits")
      isAdmin && userHasPermits
    }

}
