package io.radicalbit.rna.errors

object NotificationServiceErrors {

  implicit final val serviceName: ServiceName = ServiceName(RNAProjectIdentifiers.rtsaeNotificationsService.toString)

  implicit final val serviceCode: ServiceCode = ServiceCode(RNAProjectIdentifiers.rtsaeNotificationsService.id.toLong)

  case class ForbiddenError(details: String) extends RNAClientError {
    override val code: RNAErrorCode         = RNAErrorCodes.NotAllowedMethodErrorCode()
    override val messageArgs: Array[AnyRef] = Array(details)
  }

}
