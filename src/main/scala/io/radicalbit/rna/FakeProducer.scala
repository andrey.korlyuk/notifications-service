package io.radicalbit.rna

import java.time.Instant
import java.util.{Properties, UUID}
import com.sksamuel.avro4s.RecordFormat
import com.typesafe.config.ConfigFactory
import io.radicalbit.ls.kafka.models.{LiveShowInformation, LiveShowItem}
import io.radicalbit.rna.conf.ApplicationConfig.{KafkaConfigFactory, SchemaRegistryConfigFactory}
import io.radicalbit.rtsae.kafka.models.{Envelope, PayloadSample}
import io.radicalbit.rtsae.kafka.serde.RnaSerde.AvroValueSerialiser
import io.radicalbit.rtsae.security.kafka.serdes.Avro4sSerializer
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.serialization.Serializer
import play.api.Configuration

import scala.concurrent.{Future, Promise}
import scala.util.{Random, Try}

object Producer extends App {
  println("runnin")
  val publicProducer = new FakeProducer

  val messages        = publicProducer.createMessages(true, "show-definitions")
  val privateMessages = publicProducer.createMessages(true, "show-definitions")

  Random.shuffle(messages ++ privateMessages).foreach { e =>
    publicProducer.send(e)
    println(s"sent $e")
    Thread.sleep(1000)
  }
}

class FakeProducer() {

  val configuration                = Configuration(ConfigFactory.load())
  private val kafkaConf            = KafkaConfigFactory(configuration)
  private val schemaRegistryConfig = SchemaRegistryConfigFactory(configuration)

  def createMessages(isPublic: Boolean, key: String): Seq[Envelope] = {
    implicit val EnvelopeSerialiser: Serializer[Envelope] =
      AvroValueSerialiser(schemaRegistryConfig.url).getUnderlying[Envelope]

    implicit val ShowSerializer: Serializer[LiveShowInformation] =
      AvroValueSerialiser(schemaRegistryConfig.url).getUnderlying[LiveShowInformation]

    val testItem = LiveShowItem(UUID.randomUUID(), "name", None, None, "sku", None, "", "")

    val testMessage =
      LiveShowInformation(UUID.randomUUID(), "name", "id", "date", "date2", "Completed", true, Seq(testItem))

    val messages = (1 to 1000).map(value => testMessage.copy(name = s"val_$value"))
    //val messages = (1 to 1000).map(value => PayloadSample(UUID.randomUUID(), s"val_$value"))
    val serMsg = messages.map(m => ShowSerializer.serialize(kafkaConf.topics.head, m))
    serMsg.map(s =>
      Envelope(
        None,
        UUID.fromString("a8f0ae25-b985-4daa-94d4-e4585c062004"),
        key,
        Instant.now(),
        isPublic = isPublic,
        Seq.empty,
        //Seq("ALL"),
        s
      )
    )
  }

  protected def props: Properties = {
    val props = new Properties()
    props.put("bootstrap.servers", kafkaConf.bootstrapServers)
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer")
    props.put("value.subject.name.strategy", "io.confluent.kafka.serializers.subject.TopicRecordNameStrategy")
    props.put("auto.register.schemas", "true")
    props.put("schema.registry.url", schemaRegistryConfig.url)
    props
  }

  private lazy val producer = {
    val kp = new KafkaProducer[String, Envelope](props)
    kp
  }

  def send(event: Envelope): Future[RecordMetadata] = {
    val jFutureResult =
      //  producer.send(new ProducerRecord[String, Envelope](kafkaConf.topics.head, event))
      producer.send(new ProducerRecord[String, Envelope](kafkaConf.topics.head, "show-definitions", event))
    val promise = Promise[RecordMetadata]()

    new Thread(() =>
      promise.complete(Try {
        val result = jFutureResult.get
        result
      }.recover { case e =>
        throw new RuntimeException(e.getMessage, e)
      })
    ).start()

    promise.future
  }
}
