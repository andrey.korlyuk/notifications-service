package io.radicalbit.rna.service

import akka.NotUsed
import akka.kafka.scaladsl.Consumer
import akka.kafka.{ConsumerSettings, Subscription, Subscriptions}
import akka.stream.{Attributes, Materializer}
import akka.stream.scaladsl._
import com.typesafe.config.Config
import io.radicalbit.rna.conf.ApplicationConfig.{KafkaConfigFactory, SchemaRegistryConfigFactory}
import io.radicalbit.rna.controllers.TokenWithAdminFlag
import io.radicalbit.rtsae.kafka.models.Envelope
import io.radicalbit.rtsae.kafka.serde.RnaSerde.AvroValueDeserialiser
import io.radicalbit.rtsae.security.models.RnaToken

import javax.inject.Inject
import org.apache.avro.generic.GenericRecord
import org.apache.kafka.clients.consumer.{ConsumerConfig, ConsumerRecord}
import org.apache.kafka.common.serialization.{Deserializer, StringDeserializer}
import play.api.Configuration

import scala.util.{Failure, Success, Try}

class NotificationConsumerService @Inject() (configuration: Configuration)(implicit val mat: Materializer)
    extends BaseService {

  private val kafkaConf = KafkaConfigFactory(configuration)

  private val schemaRegistryConfig = SchemaRegistryConfigFactory(configuration)

  private val subscription: Subscription = Subscriptions.topics(kafkaConf.topics.toSet)

  private val genericRecordDeserialiser: Deserializer[GenericRecord] =
    AvroValueDeserialiser(schemaRegistryConfig.url).getUnderlying[GenericRecord]

  private val consumerSettings =
    kafkaConsumerSettings(configuration.underlying.getConfig("akka.kafka.consumer"), kafkaConf.bootstrapServers)

  log.debug(
    s"Opening Kafka stream with consumerSettings props [${consumerSettings.properties}] and subscription [$subscription]"
  )

  private val notificationConsumer = Consumer.plainSource(consumerSettings, subscription)

  private val runnableGraph = notificationConsumer.toMat(BroadcastHub.sink)(Keep.right)

  protected val broadcastingSource = runnableGraph.run()

  // Drains the queue to avoid backpressure issues and queue overflow
  broadcastingSource.to(Sink.ignore).run()

  val publicSource: Source[ConsumerRecord[String, Envelope], NotUsed] = broadcastingSource.via(privateFilterFlow(true))

  val restrictedSource: Source[ConsumerRecord[String, Envelope], NotUsed] =
    broadcastingSource.via(privateFilterFlow(false))

  def restrictedMessageFilterFlow(key: String, tokenWithFlag: TokenWithAdminFlag) =
    Flow[ConsumerRecord[String, Envelope]]
      .via(messageKeyFilterFlow(key))
      .via(subtenantFilterFlow(tokenWithFlag))
      .via(permissionFilterFlow(tokenWithFlag.token))
      .via(messageSerializer)

  def publicMessageFilterFlow(key: String): Flow[ConsumerRecord[String, Envelope], GenericRecord, NotUsed] =
    Flow[ConsumerRecord[String, Envelope]]
      .via(messageKeyFilterFlow(key))
      .via(messageSerializer)

  def queryFilterFlow(query: Map[String, Seq[String]]): Flow[GenericRecord, GenericRecord, NotUsed] =
    Flow[GenericRecord].filter { record =>
      query
        .map { case (key, acceptedValues) =>
          Try(record.get(key))
            .map(value => acceptedValues.contains(value.toString))
        }
        .map {
          case Failure(_)     => false
          case Success(value) => value
        }
        .fold(true)(_ & _)
    }

  def loggingFlow(name: String): Flow[ConsumerRecord[String, Envelope], ConsumerRecord[String, Envelope], NotUsed] =
    Flow[ConsumerRecord[String, Envelope]]
      .log(name)
      .addAttributes(
        Attributes.logLevels(
          onElement = Attributes.LogLevels.Debug,
          onFinish = Attributes.LogLevels.Info,
          onFailure = Attributes.LogLevels.Error
        )
      )

  private def messageKeyFilterFlow(key: String) =
    Flow[ConsumerRecord[String, Envelope]].filter(_.key() == key)

  private def subtenantFilterFlow(tokenWithFlag: TokenWithAdminFlag) =
    Flow[ConsumerRecord[String, Envelope]].filter { record =>
      record.value().subtenantUUID == tokenWithFlag.token.payload.rna.subtenant || tokenWithFlag.isAdmin
    }

  private def privateFilterFlow(isPublic: Boolean) =
    Flow[ConsumerRecord[String, Envelope]].filter(_.value().isPublic == isPublic)

  private def permissionFilterFlow(token: RnaToken) =
    Flow[ConsumerRecord[String, Envelope]].filter { record =>
      record.value().permits.exists(permit => token.payload.rna.permits.contains(permit))
    }

  // Last, common
  def messageSerializer: Flow[ConsumerRecord[String, Envelope], GenericRecord, NotUsed] =
    Flow[ConsumerRecord[String, Envelope]].map { record =>
      genericRecordDeserialiser.deserialize(record.topic(), record.value().payload)
    }

  private[this] def kafkaConsumerSettings(
      config: Config,
      bootstrapServers: String
  ): ConsumerSettings[String, Envelope] = {
    log.debug(s"Configuring Kafka: connection to $bootstrapServers")

    val deserialiser = AvroValueDeserialiser(schemaRegistryConfig.url, useSpecificReader = true).getUnderlying[Envelope]

    ConsumerSettings(config, new StringDeserializer, deserialiser)
      .withBootstrapServers(bootstrapServers)
      .withGroupId(config.getString("group.id"))
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, config.getString("auto.offset.reset"))
      .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false")
      .asInstanceOf[ConsumerSettings[String, Envelope]]
  }
}
