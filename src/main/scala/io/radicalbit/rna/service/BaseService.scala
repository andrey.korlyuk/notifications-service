package io.radicalbit.rna.service

import org.slf4j
import play.api.Logger

trait BaseService {
  final lazy implicit val log: slf4j.Logger = Logger(this.getClass).logger
}
