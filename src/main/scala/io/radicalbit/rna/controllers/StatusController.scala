package io.radicalbit.rna.controllers

import javax.inject.{Inject, Singleton}
import play.api.mvc._

@Singleton
class StatusController @Inject() (components: ControllerComponents) extends AbstractController(components) {

  def healthcheck: Action[AnyContent] = Action {
    Ok("running...")
  }

}
