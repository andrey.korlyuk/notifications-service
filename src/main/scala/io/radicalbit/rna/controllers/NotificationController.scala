package io.radicalbit.rna.controllers

import akka.actor.ActorRef
import akka.pattern.pipe
import akka.stream.scaladsl.{Flow, Sink}
import com.google.inject.{Inject, Singleton}
import io.radicalbit.rna.actors.WsMonitor.WsStarted
import io.radicalbit.rna.client.SecurityStorageClient
import io.radicalbit.rna.service.NotificationConsumerService
import io.radicalbit.rtsae.kafka.genericrecord.Enrichers._
import io.radicalbit.rtsae.security.clients.KeycloakClient
import io.radicalbit.rtsae.security.models.Get
import play.api.libs.json.JsValue
import play.api.mvc.{ControllerComponents, WebSocket}

import javax.inject.Named
import scala.concurrent.{ExecutionContext, Future}

@Singleton
class NotificationController @Inject() (
    controllerComponents: ControllerComponents,
    notificationConsumer: NotificationConsumerService,
    @Named("ws-monitor-actor") monitorActor: ActorRef
)(implicit ec: ExecutionContext, keycloakClient: KeycloakClient, securityStorageClient: SecurityStorageClient)
    extends BaseController(controllerComponents, securityStorageClient) {

  import io.radicalbit.rna.conf.RuntimeConfig._
  import io.radicalbit.rna.security.RnaWebSocketSecurity._

  def publicTopicWS(messageKey: String): WebSocket = WebSocket.accept[Any, JsValue] { request =>
    wsPublicNotificationsFlow(messageKey, request.queryString)
  }

  def restrictedTopicWS(messageKey: String): WebSocket = WebSocket.acceptOrResult[Any, JsValue] { request =>
    request.handleAction(Get) { rnaToken =>
      for {
        tokenWithAdminFlag <- securityRoleCheck(rnaToken)
        result <- Future.successful(
          Right(wsProtectedNotificationsFlow(messageKey, tokenWithAdminFlag, request.queryString))
        )
      } yield result
    }
  }

  private def wsProtectedNotificationsFlow(
      messageKey: String,
      token: TokenWithAdminFlag,
      query: Map[String, Seq[String]]
  ): Flow[Any, JsValue, _] = {
    monitorActor ! WsStarted
    Flow
      .fromSinkAndSource(
        Sink.ignore,
        notificationConsumer.restrictedSource
          .via(notificationConsumer.loggingFlow("protectedNotificationsStream"))
          .via(notificationConsumer.restrictedMessageFilterFlow(messageKey, token))
          .via(notificationConsumer.queryFilterFlow(query))
          .watchTermination() { (_, completionStage) =>
            completionStage pipeTo monitorActor
          }
      )
      .map(_.toJson)
  }

  private def wsPublicNotificationsFlow(messageKey: String, query: Map[String, Seq[String]]): Flow[Any, JsValue, _] = {
    monitorActor ! WsStarted
    Flow
      .fromSinkAndSource(
        Sink.ignore,
        notificationConsumer.publicSource
          .via(notificationConsumer.loggingFlow("publicNotificationsStream"))
          .via(notificationConsumer.publicMessageFilterFlow(messageKey))
          .via(notificationConsumer.queryFilterFlow(query))
          .watchTermination() { (_, completionStage) =>
            completionStage pipeTo monitorActor
          }
      )
      .map(_.toJson)
  }
}
