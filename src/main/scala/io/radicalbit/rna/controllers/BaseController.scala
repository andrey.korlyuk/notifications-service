package io.radicalbit.rna.controllers

import io.radicalbit.rna.client.SecurityStorageClient
import io.radicalbit.rtsae.security.models.RnaToken
import org.slf4j
import play.api.Logger
import play.api.mvc.{AbstractController, ControllerComponents}

import scala.concurrent.{ExecutionContext, Future}

abstract class BaseController(
    controllerComponents: ControllerComponents,
    securityStorageClient: SecurityStorageClient
)(implicit executor: ExecutionContext)
    extends AbstractController(controllerComponents) {

  final lazy val log: slf4j.Logger = Logger(this.getClass).logger

  val TotalCountHeader = "X-Total-Count"

  protected def securityRoleCheck(token: RnaToken): Future[TokenWithAdminFlag] =
    securityStorageClient
      .isAdmin(token.payload.rna.subtenant)
      .map(admin =>
        if (admin) TokenWithAdminFlag(token, isAdmin = true) else TokenWithAdminFlag(token, isAdmin = false)
      )
}

case class TokenWithAdminFlag(token: RnaToken, isAdmin: Boolean)
