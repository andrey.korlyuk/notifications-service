package io.radicalbit.rna.security

import akka.stream.scaladsl.Flow
import io.radicalbit.rtsae.security.clients.KeycloakClient
import io.radicalbit.rtsae.security.helpers.TokenValidator
import io.radicalbit.rtsae.security.models.{AllowedAction, RnaToken, ServiceExecutionEnvironment}
import play.api.http.HeaderNames
import play.api.mvc.Results.{BadRequest, Forbidden}
import play.api.mvc.{RequestHeader, Result}

import scala.concurrent.{ExecutionContext, Future}

object RnaWebSocketSecurity {

  implicit class RequestHandlers(request: RequestHeader)(implicit
      val ec: ExecutionContext,
      val keycloakClient: KeycloakClient
  ) extends TokenValidator {

    private val noAuthenticationHeaderResult: Future[Either[Result, Nothing]] =
      Future.successful(Left(BadRequest("No Authorization header.")))

    private val actionDeniedResult: Future[Left[Result, Nothing]] = Future.successful(
      Left(Forbidden("Operation not allowed in this environment."))
    )

    private def extractRnaFields[In, Out](
        f: RnaToken => Future[Either[Result, Flow[In, Out, _]]]
    ): Future[Either[Result, Flow[In, Out, _]]] =
      injectToken {
        case Some(bearerToken) =>
          val token = bearerToken.replace("Bearer ", "")
          validateToken(token).flatMap {
            case Left(ex)         => Future(Left(ex.result))
            case Right(rnaFields) => f(rnaFields)
          }
        case None =>
          noAuthenticationHeaderResult
      }

    private def allowedActionPerEE(executionEnvironment: ServiceExecutionEnvironment, action: AllowedAction) =
      executionEnvironment.actions.contains(action)

    private def injectToken[In, Out](
        f: Option[String] => Future[Either[Result, Flow[In, Out, _]]]
    ): Future[Either[Result, Flow[In, Out, _]]] =
      f(request.headers.get("Sec-WebSocket-Protocol"))

    def handleAction[In, Out](action: AllowedAction)(
        f: RnaToken => Future[Either[Result, Flow[In, Out, _]]]
    )(implicit ee: ServiceExecutionEnvironment): Future[Either[Result, Flow[In, Out, _]]] =
      extractRnaFields { rnaSecurityFields =>
        if (allowedActionPerEE(ee, action)) f(rnaSecurityFields)
        else actionDeniedResult
      }

  }
}
