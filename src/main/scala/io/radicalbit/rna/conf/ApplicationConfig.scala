package io.radicalbit.rna.conf

import com.typesafe.config.ConfigFactory
import play.api.Configuration

import scala.jdk.CollectionConverters._

object ApplicationConfig {

  private lazy val config = ConfigFactory.load()

  object SecurityStorageService {
    private lazy val host = config.getString("service.security-storage.host")
    private lazy val port = config.getInt("service.security-storage.port")

    // /api/v0.1/subtenants/admin
    private lazy val activeAdminSubtenantsUri =
      config.getString("service.security-storage.active-admin-subtenants-uri")

    lazy val activeAdminSubtenantsUrl =
      s"http://$host:$port$activeAdminSubtenantsUri"

    lazy val managerSubtenantUrl =
      s"http://$host:$port$activeAdminSubtenantsUri/manager"

    lazy val adminSubtenantUrl =
      s"http://$host:$port$activeAdminSubtenantsUri/system"
  }

  object KafkaConfigFactory {

    final case class KafkaConfig(
        bootstrapServers: String,
        topics: List[String],
        topicPartitions: Int,
        topicReplicas: Int,
        topicRetentionMs: Long
    )

    def apply(config: Configuration): KafkaConfig = {
      val kafkaConf = config.underlying.getConfig("kafka")
      KafkaConfig(
        kafkaConf.getString("bootstrap.servers"),
        kafkaConf.getString("topic.names").split(",").toList,
        kafkaConf.getInt("partitions"),
        kafkaConf.getInt("replicas"),
        kafkaConf.getLong("topic.retentionMs")
      )
    }
  }

  object SchemaRegistryConfigFactory {
    final case class SchemaRegistryConfig(url: String)

    def apply(config: Configuration): SchemaRegistryConfig = {
      val registryConf = config.underlying.getConfig("schema-registry")
      SchemaRegistryConfig(registryConf.getString("url"))
    }
  }
}
