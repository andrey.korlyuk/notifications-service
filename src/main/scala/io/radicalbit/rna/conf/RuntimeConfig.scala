package io.radicalbit.rna.conf

import com.typesafe.config.ConfigFactory
import io.radicalbit.rtsae.security.models.{Get, RnaExecutionEnvironment, ServiceExecutionEnvironment}

object RuntimeConfig {

  private lazy val config = ConfigFactory.load()

  implicit val ee: ServiceExecutionEnvironment = ServiceExecutionEnvironment(
    ee = RnaExecutionEnvironment(config.getString("service.execution-environment").toLowerCase),
    actions = Seq(Get)
  )
}
