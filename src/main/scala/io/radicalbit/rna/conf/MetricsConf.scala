package io.radicalbit.rna.conf

object MetricsConf {
  final val WsOpenedConnections = "rtsae_notifications_service_ws_connections_total"
}
