package io.radicalbit.rna.actors

import akka.actor.SupervisorStrategy.{Escalate, Resume}
import akka.actor._

final class AugmentedDefaultSupervisionStrategy extends SupervisorStrategyConfigurator {

  override def create(): SupervisorStrategy =
    OneForOneStrategy() {
      case _: ArithmeticException =>
        Resume
      case unmanagedException =>
        SupervisorStrategy.defaultDecider.applyOrElse(unmanagedException, (_: Any) => Escalate)
    }
}
