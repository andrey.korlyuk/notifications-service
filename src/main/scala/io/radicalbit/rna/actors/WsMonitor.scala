package io.radicalbit.rna.actors

import akka.Done
import akka.actor.{Actor, ActorLogging, Status}
import io.radicalbit.rna.conf.MetricsConf.WsOpenedConnections
import kamon.Kamon

class WsMonitor extends Actor with ActorLogging {
  import WsMonitor._

  private val wsConnectionsMetric = Kamon.gauge(WsOpenedConnections).withoutTags()

  override def receive: Receive = {
    case WsStarted =>
      log.debug("started monitoring of a new stream socket connection")
      wsConnectionsMetric.increment()
    case Done =>
      log.debug("stream socket connection completed correctly")
      wsConnectionsMetric.decrement()
    case Status.Failure(e) =>
      log.warning(s"stream socket connection failed: ${e.getMessage}")
      wsConnectionsMetric.decrement()
  }
}

object WsMonitor {
  case object WsStarted
}
