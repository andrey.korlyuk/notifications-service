node {
    def sbtHome = "${tool 'Sbt-1.1.6'}"

    def app
    def projectVersion
    def projectName
    def isTag
    def dependenciesFile

    stage('Clone repository') {
        checkout scm

        def buildFile = readFile 'build.sbt'
        projectName = buildFile.split("\n")
            .find { row -> row.contains('name :=') }
            .trim()
            .replaceAll("name := ", "")
            .replaceAll("\"", "")
            .replaceAll(",", "")

        def versionFile = readFile 'version.sbt'
        projectVersion = versionFile.trim()
            .replaceAll("version in ThisBuild := ", "")
            .replaceAll("\"", "")

        isTag = sh(returnStdout: true, script: "git tag --contains | head -1").trim()

        if(isTag){
            dependenciesFile="dependencies.prod.json"
        } else {
            dependenciesFile="dependencies.dev.json"
        }

        env.sbt= "${sbtHome}/bin/sbt -J-Xss64m -no-colors -batch -Ddependencies.file=${dependenciesFile}"

        echo "PROJECT NAME: ${projectName}"
        echo "PROJECT VERSION: ${projectVersion}"
        echo "DEPENDENCIES FILE: ${dependenciesFile}"
    }

    stage('Running tests') {
        sh "${sbt} clean coverage test coverageReport"
    }

    stage('Publish scoverage report') {
        step([$class: 'ScoveragePublisher', reportDir: 'target/scala-2.13/scoverage-report', reportFile: 'scoverage.xml'])
    }

    stage('Create docker image') {
        withCredentials([usernamePassword(
            credentialsId: "rb-docker-registry",
            usernameVariable: "USER",
            passwordVariable: "PASS"
        )]) {
            sh "docker login tools.radicalbit.io -u $USER -p $PASS"
            sh "${sbt} docker:publishLocal"
            if(isTag){
                sh "docker tag tools.radicalbit.io/${projectName}:${projectVersion} tools.radicalbit.io/${projectName}:latest"
            }
            if(env.BRANCH_NAME == 'develop' || env.BRANCH_NAME.startsWith('branch-') || env.BRANCH_NAME.startsWith('release-')){
                sh "docker tag tools.radicalbit.io/${projectName}:${projectVersion} tools.radicalbit.io/${projectName}:${env.BRANCH_NAME}"
            }
        }
    }

    if(env.BRANCH_NAME == 'develop' || env.BRANCH_NAME.startsWith('branch-') || env.BRANCH_NAME.startsWith('release-') || isTag){
      stage('Push docker image') {
        app = docker.image("tools.radicalbit.io/${projectName}:${projectVersion}")
        docker.withRegistry('https://tools.radicalbit.io', 'rb-docker-registry') {
            app.push(projectVersion)
            if(isTag){
                app.push("latest")
            }
            if(env.BRANCH_NAME == 'develop' || env.BRANCH_NAME.startsWith('branch-') || env.BRANCH_NAME.startsWith('release-')){
                app.push(env.BRANCH_NAME)
            }
        }
      }
    }

    if(env.BRANCH_NAME == 'develop'){
        stage('Update DEV environment'){
            build job: '../../Pipelines/CD/rna-k8s-cd', propagate: false, wait: false, parameters: [[$class: 'StringParameterValue', name: 'environment', value: 'rb-dev-gcp'],[$class: 'StringParameterValue', name: 'stack', value: 'rna'],[$class: 'StringParameterValue', name: 'service', value: projectName.replaceAll("rtsae-", "")],[$class: 'StringParameterValue', name: 'action', value: 'update']]
        }
    }

    /*
        if(env.BRANCH_NAME.startsWith('branch-') || env.BRANCH_NAME.startsWith('release-')){
            stage('Update UAT environment'){
                build job: '../../Pipelines/CD/rna-k8s-cd', propagate: false, wait: false, parameters: [[$class: 'StringParameterValue', name: 'environment', value: 'rb-uat-gcp'],[$class: 'StringParameterValue', name: 'stack', value: 'rna'],[$class: 'StringParameterValue', name: 'service', value: projectName.replaceAll("rtsae-", "")],[$class: 'StringParameterValue', name: 'action', value: 'update']]
            }
        }
    */

    stage('Remove docker data') {
        sh "docker rmi tools.radicalbit.io/${projectName}:${projectVersion}"
        if(isTag){
            sh "docker rmi tools.radicalbit.io/${projectName}:latest"
        }
        if(env.BRANCH_NAME == 'develop' || env.BRANCH_NAME.startsWith('branch-') || env.BRANCH_NAME.startsWith('release-')){
            sh "docker rmi tools.radicalbit.io/${projectName}:${env.BRANCH_NAME}"
        }
        sh "docker system prune -f"
    }
}